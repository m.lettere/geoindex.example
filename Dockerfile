FROM python:3
WORKDIR /usr/src/app
COPY __main__.py requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
CMD ["python3", ".", ">", "output.log"]
